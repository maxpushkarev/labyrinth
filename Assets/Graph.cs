﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Graph : MonoBehaviour {
	[SerializeField]
	private float animationTimeout = 0.1f;
	[SerializeField]
	private GameObject cellParent;
	[SerializeField]
	private GridLayoutGroup graphGrid;
	[SerializeField]
	private Node nodeTemplate;
	[SerializeField]
	private RectTransform rect;

	public GameObject CellParent {
		get {
			return this.cellParent;
		}
		set {
			cellParent = value;
		}
	}

	public GridLayoutGroup Grid {
		get {
			return this.graphGrid;
		}
	}

	public Node NodeTemplate {
		get {
			return this.nodeTemplate;
		}
	}

	public RectTransform Rect {
		get {
			return this.rect;
		}
	}

	public void Clear()
	{
		GameObject.DestroyImmediate (cellParent);
	}

	public void Play() {
		StopAllCoroutines ();
		StartCoroutine (Build ());
	}

	public IEnumerator Build()
	{
		Clear ();
		GameObject newParent = new GameObject ("Parent");
		newParent.transform.SetParent (transform);
		cellParent = newParent;

	
		float width = rect.rect.width;
		float height = rect.rect.height;

		float cellWidth = graphGrid.cellSize.x + graphGrid.spacing.x;
		float cellHeight = graphGrid.cellSize.y + graphGrid.spacing.y;

		RectTransform parentRect = newParent.AddComponent<RectTransform> ();
		parentRect.localPosition = Vector3.zero;
		parentRect.localRotation = Quaternion.identity;
		parentRect.offsetMax = Vector2.zero;
		parentRect.offsetMin = Vector2.zero;
		parentRect.SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, width);
		parentRect.SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, height);

		GridLayoutGroup grid = newParent.gameObject.AddComponent<GridLayoutGroup> ();
		grid.padding = graphGrid.padding;
		grid.spacing = graphGrid.spacing;
		grid.cellSize = graphGrid.cellSize;
		grid.startCorner = GridLayoutGroup.Corner.UpperLeft;
		grid.startAxis = GridLayoutGroup.Axis.Horizontal;
		grid.constraint = GridLayoutGroup.Constraint.Flexible;
		grid.childAlignment = TextAnchor.UpperLeft;

		int cellHorizontalCount = Mathf.FloorToInt (width / cellWidth);
		int cellVerticalCount = Mathf.FloorToInt (height / cellHeight);
		int childCount = cellVerticalCount * cellHorizontalCount;
		int commonCounter = 0;
		Node[] nodes = new Node[childCount];

		for (int i = 0; i < cellVerticalCount; i++) {
			for (int j = 0; j < cellHorizontalCount; j++) {
				Node node = Node.Instantiate(nodeTemplate);
				node.transform.SetParent (newParent.transform);
				node.name = "Node" + commonCounter;
				node.rightBorder.SetActive (j < cellHorizontalCount - 1);
				node.bottomBorder.SetActive (i < cellVerticalCount - 1);
				nodes [commonCounter] = node;
				commonCounter++;
			}
		}

		for (int i = 0; i < childCount; i++) {
			Node node = nodes [i];
			int rightIndex = i + 1;
			int topIndex = i - cellHorizontalCount;
			int leftIndex = i - 1;
			int bottomIndex = i + cellHorizontalCount;
			node.right = node.rightBorder.activeSelf ? nodes [rightIndex] : null;
			node.bottom = node.bottomBorder.activeSelf ? nodes [bottomIndex] : null;
			node.top = (topIndex < 0) ? null : nodes [topIndex];
			node.left = (i % cellHorizontalCount) == 0 ? null : nodes[leftIndex];
		}

		return GenerateLabyrinth (nodes [0], childCount);
	}

	private IEnumerator GenerateLabyrinth(Node node, int childCount)
	{
		Stack<Node> pathStack = new Stack<Node> ();
		int notAttendedCounter = childCount;

		node.attended = true;
		notAttendedCounter--;

		while (notAttendedCounter > 0)
		{
			if (!HasNotAttendedNeighbours (node)) {
				Node nodeFromStack = pathStack.Pop ();
				node = nodeFromStack == null ? node : nodeFromStack;
			} else {
				pathStack.Push (node);
				List<Node> list = new List<Node> ();
				AddNodeToList (list, node.right);
				AddNodeToList (list, node.bottom);
				AddNodeToList (list, node.top);
				AddNodeToList (list, node.left);
				int rnd = Random.Range (0, list.Count);
				Node rndNode = list [rnd];

				if (rndNode == node.left) {
					rndNode.rightBorder.SetActive (false);
					rndNode.rightGray.SetActive (true);
				}

				if (rndNode == node.top) {
					rndNode.bottomBorder.SetActive (false);
					rndNode.bottomGray.SetActive (true);
				}

				if (rndNode == node.bottom) {
					node.bottomBorder.SetActive (false);
					node.bottomGray.SetActive (true);
				}

				if (rndNode == node.right) {
					node.rightBorder.SetActive (false);
					node.rightGray.SetActive (true);
				}

				node = rndNode;
				node.attended = true;
				notAttendedCounter--;

				yield return new WaitForSeconds (animationTimeout);
			}
		}

		Debug.Log ("Labyrinth generated");
	}

	private bool HasNotAttendedNeighbours(Node node)
	{
		if ((node.right) && (!node.right.attended)) {
			return true;
		}

		if ((node.bottom) && (!node.bottom.attended)) {
			return true;
		}

		if ((node.top) && (!node.top.attended)) {
			return true;
		}

		if ((node.left) && (!node.left.attended)) {
			return true;
		}

		return false;
	}

	private void AddNodeToList(List<Node> nodeList, Node node)
	{
		if ((node) && (!node.attended)) {
			nodeList.Add (node);
		}
	}
}
