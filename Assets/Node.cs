﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour {
	public GameObject rightBorder;
	public GameObject bottomBorder;
	public GameObject rightGray;
	public GameObject bottomGray;
	public Node right;
	public Node bottom;
	public Node top;
	public Node left;
	public bool attended = false;
}
